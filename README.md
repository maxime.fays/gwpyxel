# GWpyxel

## Installation instructions

### Instructions on CIT:

```
conda create --clone igwn --name GWpyxel-O4a
conda activate GWpyxel-O4a
```

Cloning iwgn takes about 30min. 

```
git clone https://git.ligo.org/maxime.fays/gwpyxel.git
cd ~/gwpyxel/
pip install -e .
```

### Instructions on local machine:

```bash
brew install krb5
brew install swig
brew install openssl
conda install -c conda-forge pyfftw
conda install -c conda-forge nds2-client 
conda install -c conda-forge nds2-client-swig python-nds2-client

pip install ciecplib

# on OSX:
install Xcode (via app store)

python -m ipykernel install --user --name=firstEnv


```

```bash
conda create --name GWpyxel python=3.9
conda activate GWpyxel
git clone https://git.ligo.org/maxime.fays/gwpyxel.git ~/gwpyxel
cd ~/gwpyxel/
pip install -e .

conda install nds2-client -c conda-forge
conda install nds2-client-swig python-nds2-client -c conda-forge
```

### Activate GWpyxel

```
conda activate GWpyxel
```

## Use example

```
GWpyxel --H1L1 1242369180.0 --output ~/gwpyxel_output/
GWpyxel --V1L1 1242369180.0 --output ~/gwpyxel_output/

GWpyxel --H1 1242380191.0 --L1 1242369180.0 --output ~/gwpyxel_output/
GWpyxel --H1 1242380191.0 --L1 1242369180.0 --output ~/gwpyxel_output/ --plot
GWpyxel --H1 1242380191.0 --L1 1242369180.0 --output ~/gwpyxel_output/ --plot --tfmap_stride 6.0 --tfmap_fftlen 0.5 --tfmap_snr 8.0
GWpyxel --H1 1242380191.0 --L1 1242369180.0 --output ~/gwpyxel_output/ --plot --tfmap_stride 6.0 --tfmap_fftlen 0.5 --tfmap_snr 8.0 --sim_waveform maXgnetarD --sim_hrss 2e-21 
```

Additional input parameters available with :

```
GWpyxel --help
```

Pre-baked configurations can be used with the --config options. The options supports external files as well. 

Examples to call from inside python are available in the Notebook folder.

### Training ALBUS

```bash
conda activate GWpyxel
mkdir test_review
cd test_review
```

Generate the .dag and .sub for the 4 datasets : "background", "chirps", "glitches" and "combined".
```
../gwpyxel/GWpyxel/bin/GWpyxel_generate_training --run O3a --ifos H1L1 --background_amount 10000 --chirps_amount 1000 --glitches_amount 500000 --combined_amount 1000
```

Select some gliches across a wide variety of classes and SNR.
```
python3 ../gwpyxel/GWpyxel/bin/select_GS_glitches.py 
```

Run the .dag for the "background" and "chirps" datasets.
```
cd background
condor_submit_dag background.dag

cd ../chirps
condor_submit_dag chirps.dag
```

Train alpha. It will be used to find glitches in the cross-correlated TF maps.
```
cd ..
../gwpyxel/GWpyxel/bin/GWpyxel_train_alpha --Nb_epochs 30 --batch_size 32 --background_amount 5000 --chirps_amount 5000
```
```
cd glitches
condor_submit_dag glitches.dag
```
Select some glitches to be part of the "combined" dataset. Then, remove them from the "glitches" dataset.
```
cd ..
python3 ../gwpyxel/GWpyxel/bin/generate_list_of_glitches.py
```
Run the combined.dag to generate our last dataset.
```
cd combined
condor_submit_dag combined.dag
```
Finally train ALBUS on the 4 generated datasets.
```
cd ..
../gwpyxel/GWpyxel/bin/GWpyxel_train_ALBUS --Nb_epochs 30 --batch_size 32 
```


## FAQ

If you cannot download the data, make sure that you are inputing your LIGO credential when asked to generate a Kerberos ticket.

#### Frangi got an unpexcted argument 'mode'

This happens when the version of scikit-image installed through pip is not the latest one for some reason. Fix with :

```
pip install --upgrade scikit-image
```
