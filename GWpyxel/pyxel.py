
import shortuuid
from dotted_dict import DottedDict
from pathlib import Path

from yaspin import yaspin
from termcolor import colored
from datetime import datetime
# ---- Load all modules
import configdot
import os
from pathlib import Path
import pickle
import configparser
from pycbc.noise import noise_from_string
from dotted_dict import DottedDict
import numpy as np
import functools
import operator
from configobj import ConfigObj
from astropy.convolution import Gaussian2DKernel, interpolate_replace_nans
from scipy.signal.windows import kaiser
from scipy.signal import chirp
from lalsimulation import MeasureHrss, MeasureSNR
import pycbc.psd
from skimage.filters import threshold_yen
from skimage.measure import label, regionprops, regionprops_table
import math
import matplotlib.pyplot as plt
from skimage.morphology import (erosion, dilation, opening, closing,  # noqa
                                white_tophat)
from skimage.morphology import black_tophat, skeletonize, convex_hull_image  # noqa
import socket
from pycbc import frame
import shortuuid
from scipy import ndimage as ndi
from skimage import io, morphology, img_as_bool, segmentation
from pycbc.detector import Detector
from gwpy.timeseries import TimeSeries
from gwosc import datasets
import copy
from skimage.filters import threshold_mean
from skimage.transform import rescale, resize
from scipy import ndimage as ndi
from skimage import io, morphology, img_as_bool, segmentation
from yaspin import yaspin


from GWpyxel.utils import *
from GWpyxel.const import *
import GWpyxel

import sys
sys.path.append(f"{PATH_TO_PYXEL}/MLA/")
import compute_map
import importlib
import inspect

#temporary to deal with MDC files warning
from hide_warnings import hide_warnings


from pathlib import Path

_USE_INI_DEFAULT = object()

class Pyxel():
    def __init__(self, ini="default.ini",uuid=_USE_INI_DEFAULT,verbose=_USE_INI_DEFAULT):

        self.config = read_ini_file(ini,verbose=verbose)
        # NOTE: configdot allows for cool things like this: config.data['source']._comment

        if uuid is _USE_INI_DEFAULT: uuid = shortuuid.uuid()
        if verbose is _USE_INI_DEFAULT: verbose = self.config.gwpyxel.verbose

        # Setup placeholders for the analysis
        self.data = DottedDict()
        self.data_original = DottedDict()
        self.data_untouched = DottedDict()
        self.flag = DottedDict()
        self.ifos = list()
        self.uuid = uuid

        # Create path for cache if it doesn't exist 
        # Hard coding the set of cache folder, to avoid reading from an external file
        # Would have been nice to use the info in runs/ but too much I/O when running 1000+ jobs.
        for run in ['O1','O2','O3a','O3b','O4a','O4b']:
            for ifo in ['H1','L1','V1','K1']:
                Path(f"{os.path.expanduser(self.config.cache.path)}/{run}/{ifo}").mkdir(parents=True, exist_ok=True)

        self._import_processing_methods()
                            
    def _import_processing_methods(self):
        processing_dir = os.path.join(os.path.dirname(__file__), 'processing')
        for filename in os.listdir(processing_dir):
            if filename.endswith('.py'):
                module_name = filename[:-3]
                module = importlib.import_module(f'GWpyxel.processing.{module_name}')
                for name in dir(module):
                    if not name.startswith('_'):
                        method = getattr(module, name)
                        if callable(method):
                            # Create a method with a reference to the Pyxel instance
                            setattr(self, name, self._create_method_with_self_arg(method))

    def _create_method_with_self_arg(self, method):
        def method_with_self_arg(*args, **kwargs):
            return method(self, *args, **kwargs)
        return method_with_self_arg