import sys
import imp

# ---- Path to module
module_name = vars(sys.modules[__name__])["__package__"].split(".")[0]
PATH_TO_PYXEL = imp.find_module(module_name)[1]
