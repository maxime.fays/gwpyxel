#!/usr/bin/env python3

import configargparse
import GWpyxel
from GWpyxel.pyxel import Pyxel

import os
import numpy as np
import matplotlib.pyplot as plt
import torch
import copy
import pandas as pd
from PIL import Image
import subprocess
import shlex
import csv
import glob
from tqdm import tqdm
import GWpyxel
from GWpyxel.const import PATH_TO_PYXEL

##############################################
###   Get information about the GLITCHES   ###
##############################################

current_directory = f'{os.getcwd()}/'

os.chdir(current_directory+'glitches/output/')
files = glob.glob('*.png')

H1_list = []
L1_list = []
shifts = []
names = []

os.chdir(current_directory+'glitches/output_target_maps/')

print('Start listing glitches...')
for file in tqdm(files):
    
    target_maps = glob.glob('TM_'+file[:-4]+'.npy')
    
    if(len(target_maps)!=0):
    
        index_H1 = file.find('H1')
        H1 = int(file[index_H1+3:index_H1+13])
        H1_list.append(H1)

        index_L1 = file.find('L1')
        L1 = int(file[index_L1+3:index_L1+13])
        L1_list.append(L1)

        index_shift = file.find('shift')
        shift = int(file[index_shift+6:index_H1-1])
        shifts.append(shift)

        names.append(file)    


# Save the list in a csv file
df = pd.DataFrame()
df['H1'] = H1_list
df['L1'] = L1_list
df['Shift'] = shifts
df['Filename'] = names

df.to_csv(f'{current_directory}/list_of_glitches.csv')
    
        
#################################################################################################
###   Split this list in two : glitches for the glitch dataset and for the combined dataset   ###
#################################################################################################

# 1) Save the list of files used for the glitch dataset

df = pd.DataFrame()
df['H1'] = H1_list[::2]
df['L1'] = L1_list[::2]
df['Shift'] = shifts[::2]
df['Filename'] = names[::2]

df.to_csv(f'{current_directory}/glitches_for_glitch_dataset.csv')

# 2) Save the list of files used for combined dataset
glitches_for_combined_dataset = files[1::2]

df = pd.DataFrame()
df['H1'] = H1_list[1::2]
df['L1'] = L1_list[1::2]
df['Shift'] = shifts[1::2]
df['Filename'] = names[1::2]

df.to_csv(f'{current_directory}/glitches_for_combined_dataset.csv')


# Remove the files that will be used for the combined dataset
src = current_directory+'glitches/output/'
src_target = current_directory+'glitches/output_target_maps/'

path_to_glitches_targets = f'{current_directory}/glitches_targets/'
subprocess.run(['mkdir {:s}'.format(shlex.quote(path_to_glitches_targets))], shell=True)

print('Remove the ones that will be used in the combined dataset...')
for file in tqdm(glitches_for_combined_dataset):
    
    subprocess.run(['rm {:s}'.format(shlex.quote(src+file))], shell=True)
    subprocess.run(['mv {:s} {:s}'.format(shlex.quote(src_target+'TM_'+file[:-4]+'.npy'), shlex.quote(path_to_glitches_targets))], shell=True)
