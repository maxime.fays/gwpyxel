#!/usr/bin/env python3

import configargparse
import GWpyxel
from GWpyxel.pyxel import Pyxel

from GWpyxel.const import PATH_TO_PYXEL
import os
import numpy as np
import matplotlib.pyplot as plt
import torch
import copy
import pandas as pd
from pathlib import Path
from PIL import Image
import torchvision.transforms.functional as TF

p = configargparse.ArgParser()

p.add('--H1', required=False, type=int, default=1249842592)
p.add('--L1', required=False, type=int, default=1242960589)
p.add('--duration', required=False, type=int, default=1000)
p.add('--sim_visibility', required=False, type=float, default=15.0)
p.add('--sim_waveform', required=False, type=str, default='chirp')
p.add('--sim_delay', required=False, type=int, default=100)
p.add('--sim_ra', required=False, type=float, default=2.63)
p.add('--sim_dec', required=False, type=float, default=0.76)
p.add('--sim_polarization', required=False, type=float, default=0.0)
p.add('--output', required=False, type=str, default=f'{os.getcwd()}/')
p.add('--output_target_maps', required=False, type=str, default=f'{os.getcwd()}/')
p.add('--config', required=False, type=str, default='/home/vincent.boudart/pyxel_+_albus/GWpyxel/config/')

options = p.parse_args()

###############################
###   Get a random glitch   ###
###############################

H1_glitches = pd.read_csv(f'{PATH_TO_PYXEL}/glitches/O4a/H1_glitches.csv')
L1_glitches = pd.read_csv(f'{PATH_TO_PYXEL}/glitches/O4a/L1_glitches.csv')
   
# Read the csv files and select a glitch in each detector
list_of_glitches = pd.read_csv(f'../glitches_for_combined_dataset.csv')
Nb_glitches = len(list_of_glitches)



random_index = np.random.randint(low=0, high=Nb_glitches-1)
row = list_of_glitches.iloc[random_index]

time_H1 = row['H1'].item()
time_L1 = row['L1'].item()
shift_time = row['Shift'].item()
filename = row['Filename']

H1 = H1_glitches[np.floor(H1_glitches['GPStime']).astype(int) == time_H1]['GPStime'].values.item()
L1 = L1_glitches[np.floor(L1_glitches['GPStime']).astype(int) == time_L1]['GPStime'].values.item()

decimals_H1 = H1 - time_H1
decimals_L1 = L1 - time_L1

if(decimals_H1>decimals_L1):
    decimal_shift_H1 = decimals_H1 - decimals_L1
    decimal_shift_L1 = 0.0
else:
    decimal_shift_H1 = 0.0
    decimal_shift_L1 = decimals_L1 - decimals_H1

# New time to correlate the glitches
new_time_H1 = H1-shift_time+decimal_shift_H1
new_time_L1 = L1-shift_time+decimal_shift_L1


# Initialize pyxel and import the data
pyxel = Pyxel()


pyxel.fetch_data('H1',start=new_time_H1, duration=options.duration, cache=False, source='LIGO')
pyxel.fetch_data('L1',start=new_time_L1, duration=options.duration, cache=False, source='LIGO')


##########################
###   INJECT A CHIRP   ###
##########################

print('Start injecting a chirp...')

# Generate the chirp string
energy_decrease = np.random.choice(['chirp', 'chirp_r'])
short_or_long = np.random.choice(['long','short'])
if(short_or_long=='long'):
    duration = np.random.randint(low=50, high=501)
else:
    duration = np.random.randint(low=10, high=51)
f0 = np.random.randint(low=30, high=2001)
f1 = np.random.randint(low=30, high=2001)
freq_evolution = np.random.choice(['linear', 'quadratic', 'logarithmic', 'hyperbolic'])
beta_kaiser = np.random.uniform(low=1, high=4)

chirp = energy_decrease+'~'+str(duration)+'~'+str(f0)+'~'+str(f1)+'~'+freq_evolution+'~'+'{:.2f}'.format(beta_kaiser)

# Generation of the TF map before the injection
pyxel.generate_tfmap(prewhitening='single', postwhitening=True)

tfmap_without_injection = copy.copy(pyxel.spectrogram.value)

# Injection loop
hrss_default = 5e-21
pyxel.inject_waveform(chirp, hrss=hrss_default, delay=options.sim_delay, right_ascension=options.sim_ra, declination=options.sim_dec, polarization=0, overwrite='True') # default injection

pyxel.compute_coherence()

tfmap_with_injection = copy.copy(pyxel.spectrogram.value)

mask = pyxel.simulation.mask_spectrogram
tfmap_with_injection = np.where(mask, tfmap_with_injection, 0)
tfmap_without_injection = np.where(mask, tfmap_without_injection, 0)

print(tfmap_with_injection.max(),tfmap_without_injection.max())

tfmap_diff = tfmap_with_injection-tfmap_without_injection
tfmap_diff = np.where(tfmap_diff>0, tfmap_diff, 0)

z = 0

old_visibility = np.sum(tfmap_diff)
print('Iter: 0 | Visibility : %.2f' %(old_visibility))

visibility_history = []
visibility_history.append(old_visibility)

hrss_history = []
hrss_history.append(hrss_default)

while( ((old_visibility < options.sim_visibility*0.9) or (old_visibility > options.sim_visibility*1.1)) and (z<20) ):
    
    nearest_pos, idx_pos = pyxel.find_nearest_positive(visibility_history, options.sim_visibility)
    nearest_neg, idx_neg = pyxel.find_nearest_negative(visibility_history, options.sim_visibility)

    if(nearest_pos==0):
        new_hrss = hrss_history[-1] * 2
    elif(nearest_neg==0):
        new_hrss = hrss_history[-1] / 2
    elif(idx_pos > idx_neg):
        new_hrss = (hrss_history[idx_pos] + hrss_history[idx_neg])/2
    else:
        new_hrss = (hrss_history[idx_pos] + hrss_history[idx_neg])/2
        
    hrss_history.append(new_hrss)
    
    if(new_hrss>1e-19):
        break # Go out of the loop
    
    # Overwrite the old injection by the new one    
    pyxel.inject_waveform(chirp, hrss=new_hrss, delay=options.sim_delay, right_ascension=options.sim_ra, declination=options.sim_dec, polarization=0, overwrite='True')
    
    pyxel.compute_coherence()
    
    tfmap_with_injection = pyxel.spectrogram.value
    tfmap_with_injection = np.where(mask, tfmap_with_injection, 0)
    
    tfmap_diff = tfmap_with_injection-tfmap_without_injection
    tfmap_diff = np.where(tfmap_diff>0, tfmap_diff, 0)

    old_visibility = np.sum(tfmap_diff)
    visibility_history.append(old_visibility)
    
    print('Iter: %d | Visibility : %.2f' %(z+1,old_visibility))
    z += 1


#################################
###   END OF INJECT A CHIRP   ###
#################################
    
if(new_hrss<=1e-19):

    # Final injection
    pyxel.inject_waveform(chirp, hrss=new_hrss, delay=options.sim_delay, right_ascension=options.sim_ra, declination=options.sim_dec, polarization=0, overwrite='True')

    pyxel.compute_coherence()


    pyxel.name = f'injection_wfm_{chirp}_delay_{options.sim_delay}_visibility_{options.sim_visibility}_'+filename[:-4]

    pyxel.save_plot_albus(output=options.output)

    # Generation of the target map
    active_mask = np.zeros_like(pyxel.spectrogram.value)
    active_mask = pyxel.simulation.mask_spectrogram
    #active_mask = active_mask/np.max(active_mask)

    target_map = np.zeros((2,np.size(pyxel.spectrogram.value,0),np.size(pyxel.spectrogram.value,1)))
    target_map[0,:,:] = active_mask[:,::-1]

    # --glitch--
    path_to_glitches_targets = f'../glitches/output_target_maps_cpy/'
    target_glitch = np.load(f'{path_to_glitches_targets}/TM_'+filename[:-4]+'.npy')
    target_map[1,:,:] = target_glitch[1,:,:]

    # save it for training
    np.save(f'{options.output_target_maps}/TM_injection_wfm_{chirp}_delay_{options.sim_delay}_visibility_{options.sim_visibility}_'+filename[:-4]+'.npy', target_map)

# Save the mask for postprocessing
#mask = pyxel.simulation.mask
#np.save(f'{options.output}/mask_injection_wfm_{chirp}_delay_{options.sim_delay}_visibility_{options.sim_visibility}_'+filename+'.npy', mask)
