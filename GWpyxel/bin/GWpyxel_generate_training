#!/usr/bin/env python3

import GWpyxel
from GWpyxel.const import PATH_TO_PYXEL
from GWpyxel.utils import *
import configargparse
import os
from configobj import ConfigObj
from pycbc.distributions.sky_location import UniformSky
import numpy as np
from pycbc import dq
import pathlib
from ligo.segments import utils as segmentsUtils
import operator
import pandas as pd
import copy

from gwosc import datasets
from gwosc.timeline import get_segments
from functools import reduce
from gwpy.segments import SegmentList
import argparse
from textwrap import wrap
from gwpy.time import tconvert
from humanfriendly import format_timespan,parse_timespan
import configdot
from gwpy.segments import DataQualityFlag
import itertools
import shortuuid
from GWpyxel.pyxel import Pyxel

#####################
###   ARGUMENTS   ###
#####################

p = configargparse.ArgParser(formatter_class=argparse.RawTextHelpFormatter)

p.add('--run', required=False, default=None, type=str,help='Run to be analysed. Ex:O1, O3b')
p.add('--start', required=False, default=None, type=float,help='GPS start time')
p.add('--end', required=False, default=None, type=float,help='GPS end time')
p.add('--ifos', required=False, default=None, type=str,help='IFO to use, ex: H1, H1L1...')
p.add('--background_amount', required=False, type=int,default=8000,help='Number of background TF maps to generate')
p.add('--chirps_amount', required=False, type=int,default=1000,help='Number of chirps to generate per visibility range')
p.add('--glitches_amount', required=False, type=int,default=500000,help='Number of trial for correlating glitches')
p.add('--combined_amount', required=False, type=int,default=1000,help='Number of TF maps containing both a glitch and a chirp per visibility range')
p.add('--output_dir', required=False, default=os.getcwd()+'/')


##################
###   CONFIG   ###
##################

# Import Pyxel to get the size of the maps
pyxel = Pyxel()
duration = pyxel.config.data.duration
tres=pyxel.config.tfmap.tres

options = p.parse_known_args()[0]
ifos = wrap(options.ifos, 2)


##############################
###   CREATE DIRECTORIES   ###
##############################

# ---- create analysis directories if they don't exist
for analysis_dir in ['background','chirps','glitches','combined']:
    pathlib.Path(f'{options.output_dir}{analysis_dir}').mkdir(parents=True, exist_ok=True)
    pathlib.Path(f'{options.output_dir}{analysis_dir}/output').mkdir(parents=True, exist_ok=True)
    pathlib.Path(f'{options.output_dir}{analysis_dir}/logs').mkdir(parents=True, exist_ok=True)
    
for analysis_dir in ['chirps','glitches','combined']:
    pathlib.Path(f'{options.output_dir}{analysis_dir}/output_target_maps').mkdir(parents=True, exist_ok=True)
    

########################################
###   GENERATE FOREGROUND SEGMENTS   ###
########################################
import GWpyxel.condor as gwpc


segments = gwpc.get_segments(ifos,options.start, options.end)
foreground, _ = gwpc.segments_to_blocks(segments,block_duration=pyxel.config.data.duration)

###############################
###   GENERATE BACKGROUND   ###
###############################

background = gwpc.roll_and_sample(foreground,-10,sample_size=options.background_amount)
background.to_csv(f'{options.output_dir}/background/background.txt')
print(len(background.drop_duplicates()))

# HACK, TO CHANGE MAX !!!!!
copyforeground = foreground.copy()
copyforeground['H1'] = foreground['start']
copyforeground['L1'] = foreground['start']
copyforeground.drop(columns=['start'], inplace=True)
foreground = copyforeground

###########################
###   GENERATE CHIRPS   ###
###########################

if(options.chirps_amount > 0):
    print('Start creating chirp files...')
    
    chirps = pd.DataFrame(columns=ifos)
    
    SNR = [i for i in np.arange(2,10,1)] + [i for i in np.arange(10,21,2)]
    
    # Generate a background file with the desired number of lines
    for i in range(len(SNR)*options.chirps_amount):
        
        # Generate random indexes for all the ifos in the list
        ifo_indexes = np.random.randint(0,len(foreground['duration']),len(ifos))
        
        tmp_chirps = pd.DataFrame(columns=ifos)
        
        for idx,ifo in zip(ifo_indexes,ifos):
            
            # Get the GPS time corresponding to the index for each ifo
            tmp_chirps.loc[0,ifo] = foreground.iloc[idx][ifo]
            
        chirps = pd.concat([chirps,tmp_chirps],ignore_index=True)

    chirps['sim_visibility'] = list((GWpyxel.utils.flatten([[snr]*options.chirps_amount for snr in SNR])))
    
    chirps['sim_waveform'] = list((GWpyxel.utils.flatten([['chirp']*len(SNR)*options.chirps_amount])))
    chirps['sim_delay'] = [round(np.random.uniform(np.rint(tres).astype(int), duration-100)) for _ in range(options.chirps_amount*len(SNR))]
    chirps['sim_ra'] = UniformSky().rvs(options.chirps_amount*len(SNR))['ra']
    chirps['sim_dec'] = UniformSky().rvs(options.chirps_amount*len(SNR))['dec']
    chirps['sim_polarization'] = np.round([np.arccos(np.random.rand()*2-1)*2 for _ in range(options.chirps_amount*len(SNR))],6)
 
    #chirps['duration'] = chirps.apply(lambda row: int(row.duration),axis=1)
    chirps['duration'] = list(GWpyxel.utils.flatten([[duration]*len(chirps[ifo])]))

    chirps.to_csv(f'{options.output_dir}/chirps/chirps.txt')
    
    
#############################
###   GENERATE GLITCHES   ###
#############################

if(options.glitches_amount > 0):
    print('Start creating glitch files...')
    
    glitches = pd.DataFrame()
    
    # We don't care about the GPS times here since they will be determined by the selected glitches
    for ifo in ifos:
        glitches[ifo] = list((GWpyxel.utils.flatten([[0]*options.glitches_amount])))

    #glitches['duration'] = glitches.apply(lambda row: int(row.duration),axis=1)
    glitches['duration'] = list(GWpyxel.utils.flatten([[duration]*len(glitches[ifo])]))

    glitches.to_csv(f'{options.output_dir}/glitches/glitches.txt')

    
###################################
###   GENERATE COMBINED FILES   ###
###################################

if(options.combined_amount > 0):
    print('Start creating combined files...')
    
    combined = pd.DataFrame()
    
    SNR = [i for i in np.arange(2,10,1)] + [i for i in np.arange(10,21,2)]
    
    # We don't care about the GPS times here since they will be determined by the selected glitches
    for ifo in ifos:
        combined[ifo] = list((GWpyxel.utils.flatten([[0]*len(SNR)*options.combined_amount])))

    combined['sim_visibility'] = list((GWpyxel.utils.flatten([[snr]*options.combined_amount for snr in SNR])))
    
    combined['sim_waveform'] = list((GWpyxel.utils.flatten([['chirp']*len(SNR)*options.combined_amount])))
    combined['sim_delay'] = [round(np.random.uniform(np.rint(tres).astype(int), duration-100)) for _ in range(options.combined_amount*len(SNR))]
    combined['sim_ra'] = UniformSky().rvs(options.combined_amount*len(SNR))['ra']
    combined['sim_dec'] = UniformSky().rvs(options.combined_amount*len(SNR))['dec']
    combined['sim_polarization'] = np.round([np.arccos(np.random.rand()*2-1)*2 for _ in range(options.combined_amount*len(SNR))],6)
 
    #combined['duration'] = combined.apply(lambda row: int(row.duration),axis=1)
    combined['duration'] = list(GWpyxel.utils.flatten([[duration]*len(combined[ifo])]))

    combined.to_csv(f'{options.output_dir}/combined/combined.txt')
    
    
#################################
###   GENERATE SUBMIT FILES   ###
#################################
from GWpyxel.const import PATH_TO_PYXEL

gwpc.generate_condor_files(background, f"{PATH_TO_PYXEL}/bin/GWpyxel", request_memory=4096, request_disk="10MB",additional_params='--background')

GWpyxel.utils.generate_submit_file(chirps, 'chirps', f'{options.output_dir}/chirps/', 'GWmaketfmap_chirp', target_maps=True)
GWpyxel.utils.generate_submit_file(glitches, 'glitches', f'{options.output_dir}/glitches/', 'GWmaketfmap_correlate_glitches', target_maps=True)
GWpyxel.utils.generate_submit_file(combined, 'combined', f'{options.output_dir}/combined/', 'GWmaketfmap_combined', target_maps=True)
