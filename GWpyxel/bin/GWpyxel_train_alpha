#!/usr/bin/env python3

######################
###   Librairies   ###
######################

import configargparse
import argparse
import os
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision
import torchvision.transforms as transforms
import torchvision.transforms.functional as TF
from PIL import Image
torch.backends.cudnn.benchmark=True
torch.autograd.set_detect_anomaly(True)
import numpy as np
import glob
from tqdm import tqdm

# Import Pyxel to get the size of the maps
from GWpyxel.pyxel import Pyxel
from GWpyxel.const import PATH_TO_PYXEL

pyxel = Pyxel()

duration = pyxel.config.data.duration
tres = pyxel.config.tfmap.tres
max_freq = pyxel.config.data.Fs/2
fres = pyxel.config.tfmap.fres

image_size = [int(np.floor(duration/tres)), int(np.floor(max_freq/fres)+1)]


#####################
###   ARGUMENTS   ###
#####################

p = configargparse.ArgParser(formatter_class=argparse.RawTextHelpFormatter)

p.add('--Nb_epochs', required=False, type=int, default=30)
p.add('--batch_size', required=False, type=int, default=20)
p.add('--background_amount', required=False, type=int, default=4000)
p.add('--chirps_amount', required=False, type=int, default=4000)
p.add('--output_dir', required=False, default=os.getcwd()+'/')

options = p.parse_known_args()[0]


############################
###   Paths and device   ###
############################

# Paths to the data
path_injection = os.getcwd()+'/chirps/output/'
path_target_maps = os.getcwd()+'/chirps/output_target_maps/'
path_background = os.getcwd()+'/background/output/'

# Decide which device we want to run on
device = torch.device("cuda:0" if (torch.cuda.is_available()) else "cpu")
if(device!=torch.device("cpu")):
    print('Running on GPU(s) !')
    print("GPU name: ",torch.cuda.get_device_name(0))
    ngpu = 1
else:
    print('Warning ! Running on CPU(s) !')
    ngpu = 0
    

###############################
###   Training parameters   ###
###############################
    
# Define the visibility levels
visibilities = np.asarray([i for i in np.arange(2,10,1)] + [i for i in np.arange(10,21,2)])
visibilities = visibilities[::-1] # reverse to load the easiest at first

Nb_visibilities = len(visibilities) 
Nloads = int(options.chirps_amount/Nb_visibilities)

# Ratio of data to be taken for testing
valid_ratio = 0.1

# Define the learning rate
lr = 0.0001

# Load trained model (1) or not
load_model = 0


############################
###   Sample functions   ###
############################

class SampleDataset_injection:
    def __init__(self, list_of_paths, list_of_waveforms, visibility, N, list_paths_proximity):
        # Load the data for each path
        data = []
        names = []
        for path in list_of_paths:
            for wave in list_of_waveforms:
                os.chdir(path)
                files = glob.glob('injection_wfm_chirp*'+'_visibility_'+str(float(visibility))+'*')
                files = sorted(files)
                
                dataset_torch = []
                for i in files[:N]:
                    names.append(i)
                    temp = Image.open(i)
                    torch_img = TF.to_tensor(temp)
                    torch_final = torch_img[:3,:,:].permute(0,2,1)
                    dataset_torch.append(torch_final)
                    temp.close()

                data = data + dataset_torch
            
        self.samples = data
        
        # Load the target map
        data_prox = []
        for path_proximity in list_paths_proximity:
            for wave in list_of_waveforms:
                os.chdir(path_proximity)
                files = glob.glob('TM_injection_wfm_chirp*'+'_visibility_'+str(float(visibility))+'*')
                files = sorted(files)

                #dataset_npy = [np.load(i) for i in files[:N]]
                dataset_npy = [np.load('TM_'+name[:-4]+'.npy') for name in names]
                dataset_npy = [i[0,:,::-1].copy() for i in dataset_npy]
                dataset_torch = [torch.from_numpy(i).unsqueeze(0) for i in dataset_npy]
                data_prox = data_prox + dataset_torch
                
        self.prox_maps = data_prox

 
    def __len__(self):
        return len(self.samples)
 
    def __getitem__(self,idx):
        sample = self.samples[idx]
        prox_map = self.prox_maps[idx]
        return sample, prox_map
    
    def __getall__(self):
        return self.samples, self.prox_maps

    def __add__(self, other):
        for i in np.arange(len(other)):
            other_sample, other_prox = other.__getitem__(i)
            self.samples.append(other_sample)
            self.prox_maps.append(other_prox)
        return
    
class SampleDataset_background:
    def __init__(self, list_of_paths, N):
        # Load the data for each path
        data = []
        for path in list_of_paths:
            os.chdir(path)
            files = glob.glob('background_*')
            files = sorted(files)
            
            dataset_torch = []
            for i in files[:N]:
                temp = Image.open(i)
                torch_img = TF.to_tensor(temp)
                torch_final = torch_img[:3,:,:].permute(0,2,1)
                dataset_torch.append(torch_final)
                temp.close()

            data = data + dataset_torch
            
        self.samples = data
        self.prox_maps = torch.zeros([1,image_size[0],image_size[1]], dtype=torch.float64)

    def __len__(self):
        return len(self.samples)
 
    def __getitem__(self,idx):
        sample = self.samples[idx]
        prox_map = self.prox_maps
        return sample, prox_map
    
    def __getall__(self):
        return self.samples, self.prox_maps

    def __add__(self, other):
        for i in np.arange(len(other)):
            other_sample, other_prox = other.__getitem__(i)
            self.samples.append(other_sample)
            self.prox_maps.append(other_prox)
        return


####################
###   Networks   ###
####################

NfeatureMaps = 16
activation = nn.ELU()

# Feature extractor network
class FeatureExtractor(nn.Module):
    def __init__(self):
        super(FeatureExtractor, self).__init__()
        
        self.conv1 = nn.Sequential(
            nn.Conv2d(
                in_channels=3, out_channels=NfeatureMaps, kernel_size=3, 
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps),
            nn.ELU(True)
        )
        self.strided_conv1 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps, out_channels=NfeatureMaps*2, kernel_size=3,
                stride=2, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*2),
            nn.ELU(True)
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*2, out_channels=NfeatureMaps*2, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*2),
            nn.ELU(True)
        )
        self.strided_conv2 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*2, out_channels=NfeatureMaps*4, kernel_size=3,
                stride=2, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*4),
            nn.ELU(True)
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*4, out_channels=NfeatureMaps*4, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*4),
            nn.ELU(True)
        )
        self.conv4 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*4, out_channels=NfeatureMaps*4, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*4),
            nn.ELU(True)
        )
        self.strided_conv3 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*4, out_channels=NfeatureMaps*8, kernel_size=3,
                stride=2, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*8),
            nn.ELU(True)
        )
        self.conv5 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*8, out_channels=NfeatureMaps*8, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.Dropout(p=0.5),
            nn.BatchNorm2d(NfeatureMaps*8),
            nn.ELU(True)
        )
        self.conv6 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*8, out_channels=NfeatureMaps*8, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.Dropout(p=0.5),
            nn.BatchNorm2d(NfeatureMaps*8),
            nn.ELU(True)
        )
        self.strided_conv4 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*8, out_channels=NfeatureMaps*8, kernel_size=3,
                stride=2, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*8),
            nn.ELU(True)
        )
        self.conv7 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*8, out_channels=NfeatureMaps*8, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.Dropout(p=0.5),
            nn.BatchNorm2d(NfeatureMaps*8),
            nn.ELU(True)
        )
        self.conv8 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*8, out_channels=NfeatureMaps*8, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.Dropout(p=0.5),
            nn.BatchNorm2d(NfeatureMaps*8),
            nn.ELU(True)
        )
        
        self.conv_trans1 = nn.Sequential(
            nn.ConvTranspose2d(
                in_channels=NfeatureMaps*8, out_channels=NfeatureMaps*8, kernel_size=3, 
                stride=2, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*8),
            nn.ELU(True)
        )
        self.conv9 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*16, out_channels=NfeatureMaps*16, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*16),
            nn.ELU(True)
        )
        self.conv10 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*16, out_channels=NfeatureMaps*16, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*16),
            nn.ELU(True)
        )
        self.conv_trans2 = nn.Sequential(
            nn.ConvTranspose2d(
                in_channels=NfeatureMaps*16, out_channels=NfeatureMaps*4, kernel_size=3,
                stride=2, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*4),
            nn.ELU(True)
        )
        self.conv11 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*8, out_channels=NfeatureMaps*8, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*8),
            nn.ELU(True)
        )
        self.conv12 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*8, out_channels=NfeatureMaps*8, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*8),
            nn.ELU(True)
        )
        self.conv_trans3 = nn.Sequential(
            nn.ConvTranspose2d(
                in_channels=NfeatureMaps*8, out_channels=NfeatureMaps*2, kernel_size=3,
                stride=2, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*2),
            nn.ELU(True)
        )
        self.conv13 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*4, out_channels=NfeatureMaps*4, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*4),
            nn.ELU(True)
        )
        self.conv_trans4 = nn.Sequential(
            nn.ConvTranspose2d(
                in_channels=NfeatureMaps*4, out_channels=NfeatureMaps, kernel_size=3,
                stride=2, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps),
            nn.ELU(True)
        )
        self.conv14 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*2, out_channels=NfeatureMaps*2, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*2),
            nn.ELU(True)
        )
        
        self.conv_trans_inter1 = nn.Sequential(
            nn.ConvTranspose2d(
                in_channels=NfeatureMaps*16, out_channels=NfeatureMaps*2, kernel_size=3,
                stride=8, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*2),
            nn.ELU(True)
        )
        self.conv_trans_inter2 = nn.Sequential(
            nn.ConvTranspose2d(
                in_channels=NfeatureMaps*8, out_channels=NfeatureMaps*2, kernel_size=3,
                stride=4, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps*2),
            nn.ELU(True)
        )
        
        self.conv15 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps*2, out_channels=NfeatureMaps, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.BatchNorm2d(NfeatureMaps),
            nn.ELU(True)
        )
        self.conv16 = nn.Sequential(
            nn.Conv2d(
                in_channels=NfeatureMaps, out_channels=1, kernel_size=3,
                stride=1, padding=1, bias=False
            ),
            nn.Sigmoid()
        )

    def forward(self, x):
        
        # Downscaling network
        # First Layer
        x1 = self.conv1(x)
        x2 = self.strided_conv1(x1)
        # Second Layer
        x3 = self.conv2(x2)
        x4 = activation(x3 + x2)
        x5 = self.strided_conv2(x4)
        # Third Layer
        x6 = self.conv3(x5)
        x7 = self.conv4(x6)
        x8 = activation(x7 + x5)
        x9 = self.strided_conv3(x8)
        # Fourth Layer
        x10 = self.conv5(x9)
        x11 = self.conv6(x10)
        x12 = activation(x11 + x9)
        x13 = self.strided_conv4(x12)
        # Fifth Layer
        x14 = self.conv7(x13)
        x15 = self.conv8(x14)
        x16 = activation(x15 + x13)
        
        del x15,x14,x13,x11,x10,x9,x7,x6,x5,x3,x2
        
        # Upscaling network
        # First Layer
        x17 = self.conv_trans1(x16)
        x18 = torch.cat( (x17,x12), 1)
        x19 = self.conv9(x18)
        x20 = self.conv10(x19)
        x21 = activation(x20 + x18)
        # Second Layer
        m = nn.ZeroPad2d((0,0,1,0))
        x22 = self.conv_trans2(x21)
        x22 = m(x22)
        x23 = torch.cat( (x22,x8), 1)
        x24 = self.conv11(x23)
        x25 = self.conv12(x24)
        x26 = activation(x25 + x23)
        # Third Layer
        x27 = self.conv_trans3(x26)
        x28 = torch.cat( (x27,x4), 1)
        x29 = self.conv13(x28)
        x30 = activation(x29 + x28)
        # Fourth Layer
        x31 = self.conv_trans4(x30)
        x31 = m(x31)
        x32 = torch.cat( (x31,x1), 1)
        x33 = self.conv14(x32)
        x34 = activation(x33 + x32)
        # Intermediate transposed conv
        #x1_inter = self.conv_trans_inter1(x21)
        #x2_inter = self.conv_trans_inter2(x26)
        # Putting everything together
        #x2_inter = m(x2_inter)
        #m = nn.ZeroPad2d((0,0,3,2))
        #x1_inter = m(x1_inter)
        #x35 = torch.cat( (x34, x1_inter, x2_inter), 1)
        x36 = self.conv15(x34)
        x37 = self.conv16(x36)
        
        return x37
    

##################################
###   Weights initialization   ###
##################################

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
        #nn.init.xavier_normal_(m.weight.data)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        #nn.init.xavier_normal_(m.weight.data)
        nn.init.constant_(m.bias.data, 0)

        
alpha = FeatureExtractor().to(device)

if(load_model==1):
    alpha.load_state_dict(torch.load(f'{PATH_TO_PYXEL}/MLA/state_alpha_test.pth'))
    alpha.train()
else:
    alpha.apply(weights_init)
    
    
#################################
###   Define the dataloader   ###
#################################

print('Loading the data...')

### Define the training set for the background ###
list_paths = [path_background]

trainset_ref_background = SampleDataset_background(list_paths, options.background_amount)
n_samples_background = len(trainset_ref_background)

# Split into training and testing sets
trainset, testset = torch.utils.data.random_split(trainset_ref_background, [n_samples_background-int(valid_ratio*n_samples_background), int(valid_ratio*n_samples_background)])

# Load the training dataset via the dataloader
background_loader = torch.utils.data.DataLoader(trainset, batch_size=int(options.batch_size/2), shuffle=True, num_workers=0)
background_test = torch.utils.data.DataLoader(testset, batch_size=int(options.batch_size/2), shuffle=True, num_workers=0)

del trainset
del testset
del trainset_ref_background

print("Background samples:", n_samples_background)

### Define the training set for the injections ###
list_paths = [path_injection]
list_paths_proximity = [path_target_maps]

waveforms = ['chirp']

trainset_ref = SampleDataset_injection(list_paths, waveforms, visibilities[0], Nloads, list_paths_proximity)

for visibility in visibilities[1:Nb_visibilities]:
    trainset = SampleDataset_injection(list_paths, waveforms, visibility, Nloads, list_paths_proximity)
    trainset_ref.__add__(trainset)
    
n_samples = len(trainset_ref)
    
# Split into training and testing sets
trainset, testset = torch.utils.data.random_split(trainset_ref, [n_samples-int(valid_ratio*n_samples), int(valid_ratio*n_samples)])

# Load the training dataset via the dataloader
injection_loader = torch.utils.data.DataLoader(trainset, batch_size=int(options.batch_size/2), shuffle=True, num_workers=0)
injection_test = torch.utils.data.DataLoader(testset, batch_size=int(options.batch_size/2), shuffle=True, num_workers=0)

del trainset
del testset
del trainset_ref

print("Chirp samples:", n_samples)
print("Batch size:", options.batch_size, "Validation percentage:", int(valid_ratio*100), "%")

print('Data loaded successfully !')


##########################################
#   Loss function and gradient descent   #
##########################################

# Loss function
def weighted_MSE_loss(output_map, proximity_map):
    
    losses = torch.zeros(output_map.size(0))
    
    for i in np.arange(output_map.size(0)):
        
        losses[i] = 1/2 * torch.sum( (output_map[i,0,:,:] - proximity_map[i,0,:,:])**2 )
        
    return torch.mean(losses)

# Optimizer
optimizer = torch.optim.Adam(alpha.parameters(), lr=lr)#, weight_decay=10**(-5))
#optimizer = torch.optim.Adamax(alpha.parameters(), lr=lr, weight_decay=10**(-5))
#optimizer = torch.optim.SGD(alpha.parameters(), lr=0.01, momentum=0.9, weight_decay=10**(-6), nesterov=True)


#####################
#   Training Loop   #
#####################

# Save the losses
loss_history = []
loss_history_test = []

print('Starting the training phase...')
for epoch in np.arange(options.Nb_epochs):
    
    # Set the training mode for the two networks
    alpha.train()
    
    # Counting the number of batches
    Z = 1
    
    # Average loss over the batches of the same epoch
    loss_accumulated = []
    loss_accumulated_test = []
    
    #print(torch.cuda.memory_allocated(device=device))
    
    for samples_injection, samples_background in zip(injection_loader, background_loader):
    
        # Zero the parameter gradients
        optimizer.zero_grad()
        
        # 1) Draw a batch with same number of samples for each dataloader
        batch_background, prox_maps_background = samples_background
        batch_injection, prox_maps_injection = samples_injection
 
        batch = torch.cat((batch_background, batch_injection), 0)
        prox_maps = torch.cat((prox_maps_background, prox_maps_injection), 0)
        batch = batch.to(device)
        prox_maps = prox_maps.to(device)
        
        # 2) Propagate through the network
        output_maps = alpha(batch)
        
        #print(torch.any(torch.isnan(prox_maps)))
        prox_maps = torch.nan_to_num(prox_maps, nan=0.0)
        
        # 3) Compute the loss
        loss = weighted_MSE_loss(output_maps, prox_maps)
        loss_accumulated.append(loss.item())
        
        # 4) Backward propagation
        loss.backward()
        optimizer.step()
        
        Z = Z + 1
    
        del batch, batch_background, batch_injection, prox_maps, prox_maps_background, prox_maps_injection, output_maps, samples_injection, samples_background, loss
    
    loss_mean_epoch = np.mean(loss_accumulated)
    loss_history.append(loss_mean_epoch)
    
    # TEST part
    alpha.eval()
    
    with torch.no_grad():
        for samples_injection, samples_background in zip(injection_test, background_test):

            # 1) Draw a batch with same number of samples for each dataloader
            batch_background, prox_maps_background = samples_background
            batch_injection, prox_maps_injection = samples_injection

            batch = torch.cat((batch_background, batch_injection), 0)
            prox_maps = torch.cat((prox_maps_background, prox_maps_injection), 0)
            batch = batch.to(device)
            prox_maps = prox_maps.to(device)

            # 2) Propagate through the network
            output_maps = alpha(batch)
            
            prox_maps = torch.nan_to_num(prox_maps, nan=0.0)

            # 3) Compute the loss
            loss = weighted_MSE_loss(output_maps, prox_maps)
            loss_accumulated_test.append(loss.item())

            del batch, batch_background, batch_injection, prox_maps, prox_maps_background, prox_maps_injection, output_maps, samples_injection, samples_background, loss
        
    loss_mean_epoch_test = np.mean(loss_accumulated_test)
    loss_history_test.append(loss_mean_epoch_test)
    
    print('Epoch : %d/%d | Train loss : %.4f | Validation loss : %.4f' %(epoch+1, options.Nb_epochs, loss_mean_epoch, loss_mean_epoch_test))
    
    
##################################
#   Save the losses and states   #
##################################

# Losses
loss_train = np.asarray(loss_history)
loss_valid = np.asarray(loss_history_test)
np.save(options.output_dir+'alpha_loss_train.npy', loss_train)
np.save(options.output_dir+'alpha_loss_valid.npy', loss_valid)

# State
torch.save(alpha.state_dict(), f'{PATH_TO_PYXEL}/MLA/state_alpha_test.pth')
