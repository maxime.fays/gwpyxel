import pandas as pd
import socket
import getpass
import os
import shortuuid
from varname import argname
from GWpyxel.const import PATH_TO_PYXEL


def generate_condor_files(df, executable=f"{PATH_TO_PYXEL}/bin/GWpyxel", request_memory=4096, request_disk="10MB",
                         additional_params=''):
    hostname = socket.gethostname()
    user = getpass.getuser()  # Get the current machine username
    filename = argname('df')

    # Add a 'uuid' column with unique UUIDs for each row
    df['uuid'] = [shortuuid.uuid() for _ in range(len(df))]

    # Choose the correct .sub file based on the hostname
    if 'ldas' in hostname:
        base_sub_file_path =  f"{PATH_TO_PYXEL}/condor/ldas.sub"
    else:
        base_sub_file_path = f"{PATH_TO_PYXEL}/condor/default.sub"

    # Read the base .sub file content
    with open(base_sub_file_path, 'r') as file:
        sub_content = file.read()

    # Replace placeholders in the .sub file content
    sub_content = sub_content.replace("VAR_EXECUTABLE", executable)
    sub_content = sub_content.replace("VAR_REQUEST_MEMORY", str(request_memory))
    sub_content = sub_content.replace("VAR_REQUEST_DISK", request_disk)
    sub_content = sub_content.replace("VAR_USER", user)

    num_columns = len(df.columns)
    # Generate the macro arguments string
    macro_arguments_str = " ".join([f"$(macroargument{i})" for i in range(num_columns)]) 
    # add additional params 
    macro_arguments_str += f" {additional_params}"
    # Replace VAR_ARGUMENTS in the .sub file content
    sub_content = sub_content.replace("VAR_ARGUMENTS", macro_arguments_str)
    

    # Write the modified .sub file
    sub_file_name = f"{filename}.sub"
    with open(sub_file_name, "w") as sub_file:
        sub_file.write(sub_content)

    # Prepare the .dag file content
    dag_content = ""
    for index, row in df.iterrows():
        job_name = f"{filename}_{row['uuid']}"  # Use dataframe_name_uuid as the job name
        vars_values = " ".join([f'macroargument{i}="--{col} {row[col]}"' for i, col in enumerate(df.columns)])


        dag_content += f"JOB {job_name} {sub_file_name}\nVARS {job_name} {vars_values}\n"

    # Write the .dag file
    dag_file_name = f"{filename}.dag"
    with open(dag_file_name, "w") as dag_file:
        dag_file.write(dag_content)

    print(f"Generated {sub_file_name} and {dag_file_name} files.")
