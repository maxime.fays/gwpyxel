import configparser

import logging
logger = logging.getLogger(__name__)


_USE_INI_DEFAULT = object()

from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message
import GWpyxel.utils
from GWpyxel.const import PATH_TO_PYXEL
from functools import reduce
from pycbc import dq

@time_logger
def get_segments(ifos, start,end):
    run = GWpyxel.utils.run_at_gps(start)
    era = configparser.ConfigParser()
    era.read(f"{PATH_TO_PYXEL}/runs/era.ini")
    veto = era.get(run, "veto") 

    obsRun = configparser.ConfigParser()
    obsRun.read(f"{PATH_TO_PYXEL}/runs/{run}.ini")
    segments = {}
    for ifo in ifos:
        channel = obsRun.get(ifo, "channel") 
        flag = obsRun.get(ifo, "flag")
        segments[ifo] = dq.query_str(ifo, f'+{flag}',start, end, source='dqsegdb',
                 server='https://segments.ligo.org',veto_definer=f"{PATH_TO_PYXEL}/runs/vetoes/{veto}")

    coinc_segments = reduce(lambda a, b: a & b, segments.values())
    return coinc_segments