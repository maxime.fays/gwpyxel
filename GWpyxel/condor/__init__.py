# GWpyxel/condor/__init__.py
import os
import importlib

# Get the directory of the current file (__init__.py)
current_dir = os.path.dirname(__file__)

# List all python files in the directory
module_files = [f for f in os.listdir(current_dir) if f.endswith('.py') and f != '__init__.py']

# Import each module and its functions
for filename in module_files:
    module_name = filename[:-3]  # Remove '.py' from filename
    module = importlib.import_module('.' + module_name, package='GWpyxel.condor')
    
    # Add each function from the module to the global namespace of the package
    for attr in dir(module):
        if callable(getattr(module, attr)) and not attr.startswith('_'):
            globals()[attr] = getattr(module, attr)
