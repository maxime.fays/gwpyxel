import numpy as np
import scipy.signal
import matplotlib.pyplot as plt
from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message

import logging
logger = logging.getLogger(__name__)

_USE_INI_DEFAULT = object()


@time_logger
def postwhitening(self):

    whitening_factor = self.spectrogram.value.sum(axis=0)[None, :]
    whitening_factor[np.where(whitening_factor == 0)] = np.inf
    self.spectrogram = self.spectrogram/(whitening_factor)


    set_logging_message(f"spectrogram normalized")


