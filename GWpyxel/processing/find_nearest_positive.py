    
def find_nearest_positive(self, list_array, value):
    value_rounded = round(value, 3)
    list_array_rounded = [round(x, 3) for x in list_array]
    list_reduced = [x - value_rounded for x in list_array_rounded]
    list_positive = [x for x in list_reduced if x>0]
    if(list_positive==[]):
        return 0, 0
    nearest_positive = min(list_positive)+value_rounded
    idx_nearest = list_array_rounded.index(round(nearest_positive,3))
    return nearest_positive, idx_nearest