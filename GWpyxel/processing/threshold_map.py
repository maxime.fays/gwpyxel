import numpy as np
from skimage.filters import threshold_yen

import logging
logger = logging.getLogger(__name__)


_USE_INI_DEFAULT = object()

from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message
@time_logger
def threshold_map(self,mmap):
    regions = mmap > np.maximum(threshold_yen(mmap, nbins=256, hist=None),0.005)
    #regions = mmap > np.maximum(threshold_yen(mmap, nbins=256, hist=None)/2,0.001)
    #regions = mmap > np.percentile(mmap, 99.99)
    regions[self.spectrogram.frequencies.value<self.config.data.minfreq,:] = 0
    regions[self.spectrogram.frequencies.value>=self.config.data.maxfreq,:] = 0
    from skimage import morphology
    morphology.remove_small_objects(regions, 3, connectivity=2,out=regions)


    if self.config.data.remove_first_x_seconds:
        times = self.spectrogram.times.value-self.spectrogram.t0.value
        mmap[:,times<=self.config.data.remove_first_x_seconds] = 0
        #self.Glitch[:,times<=self.config.data.remove_first_x_seconds] = 0
        regions[:,times<=self.config.data.remove_first_x_seconds] = 0


    from scipy import ndimage as ndi
    from skimage import io, morphology, img_as_bool, segmentation
    from skimage.morphology import erosion
    from skimage.morphology import disk
    mask = ndi.distance_transform_edt(~regions,sampling=[0.2*self.config.tfmap.fres,0.7*self.config.tfmap.tres])
    #mask = ndi.distance_transform_edt(~regions,sampling=[0.7*self.config.tfmap.fres,0.7*self.config.tfmap.tres])
    mask = mask <= 5
    return mask            