import numpy as np
import scipy.signal
import matplotlib.pyplot as plt
from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message

import logging
logger = logging.getLogger(__name__)

_USE_INI_DEFAULT = object()


@time_logger
def compute_coherence(self,signal1=_USE_INI_DEFAULT, signal2=_USE_INI_DEFAULT, fs=_USE_INI_DEFAULT, segment_duration=_USE_INI_DEFAULT, freq_res=_USE_INI_DEFAULT,
                     prewhitening=_USE_INI_DEFAULT,postwhitening=_USE_INI_DEFAULT):

    if fs is _USE_INI_DEFAULT: fs = self.config.data.Fs
    if segment_duration is _USE_INI_DEFAULT: segment_duration = self.config.tfmap.tres
    if freq_res is _USE_INI_DEFAULT: freq_res = self.config.tfmap.fres
    if signal1 is _USE_INI_DEFAULT: signal1 = self.data[self.ifos[0]].value
    if signal2 is _USE_INI_DEFAULT: signal2 = self.data[self.ifos[1]].value
    if prewhitening is _USE_INI_DEFAULT: prewhitening = self.config.whitening.prewhitening
    if postwhitening is _USE_INI_DEFAULT: postwhitening = self.config.whitening.postwhitening
                
        
    segment_samples = int(fs * segment_duration)
    nfft = int(fs / freq_res)
    coherence_values = []

    if prewhitening:
        self.prewhitening()

    # Compute coherence for each segment
    for start in range(0, len(signal1), segment_samples):
        end = start + segment_samples
        # Ensure that both signals have the same length segments
        if end > len(signal1) or end > len(signal2):
            break  # Skip the last segment if it's shorter than desired

        # Extract segments for both signals
        segment1 = signal1[start:end]
        segment2 = signal2[start:end]

        # Compute the coherence between the two segments
        f, Cxy = scipy.signal.coherence(segment1, segment2, fs, window='hann', nperseg=nfft, detrend='constant')
        coherence_values.append(Cxy)

    # Stack coherence estimates to form a time-frequency coherence map
    from gwpy.spectrogram import Spectrogram 
    specgram = Spectrogram(np.swapaxes(np.array(coherence_values).T,0,1),epoch=self.data[self.ifos[0]].t0,
                      unit='coherence', dt=self.config.tfmap.tres, df=self.config.tfmap.fres) 
    self.spectrogram = specgram

    if postwhitening:
        self.postwhitening()