import logging
logger = logging.getLogger(__name__)
import matplotlib.pyplot as plt
import numpy as np

_USE_INI_DEFAULT = object()

from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message
@time_logger
def plot(self):
    fig, ax = plt.subplots(figsize=(19,9))
    plt.imshow(self.spectrogram.value.T, cmap='bone' ,origin='lower',aspect='auto',interpolation='none', 
              extent=[0,self.spectrogram.times[-1].value-self.spectrogram.times[0].value,self.spectrogram.frequencies[0].value,self.spectrogram.frequencies[-1].value])
    
    try:
        plt.imshow(self.TFmap, cmap='afmhot' ,origin='lower',aspect='auto',interpolation='none',alpha=0.999*(self.TFmap>0.03), 
              extent=[0,self.spectrogram.times[-1].value-self.spectrogram.times[0].value,self.spectrogram.frequencies[0].value,self.spectrogram.frequencies[-1].value])
        plt.contour(self.skeleton.astype(bool), cmap='brg' ,origin='lower',linewidths=2,
              extent=[0,self.spectrogram.times[-1].value-self.spectrogram.times[0].value,self.spectrogram.frequencies[0].value,self.spectrogram.frequencies[-1].value])
    except:
        pass
    ax = plt.gca()
    ax.ticklabel_format(style='plain', axis='x', useOffset=False)
    plt.title(f'From {self.spectrogram.times[0].value} to {self.spectrogram.times[-1].value}')
    plt.xlabel('Time (s)')
    plt.ylabel('Frequency (Hz)')
    plt.axis('tight')
    plt.show()


