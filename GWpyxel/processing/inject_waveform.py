import os 
from dotted_dict import DottedDict
import numpy as np
from scipy.signal.windows import kaiser
from scipy.signal import chirp
from GWpyxel.utils import *
from GWpyxel.const import *
from pycbc.detector import Detector
from gwpy.timeseries import TimeSeries
from lalsimulation import MeasureHrss, MeasureSNR

import logging
logger = logging.getLogger(__name__)


_USE_INI_DEFAULT = object()

from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message

@time_logger
def inject_waveform(
    self,
    waveform,
    hrss=None,
    SNR=None,
    right_ascension=2.63,
    declination=0.76,
    polarization=0,
    delay=1,
    catalog=None,
    overwrite='False',
):
    if catalog is None:
        catalog = self.config.waveforms.catalog

    # ---- Set all attributes
    self.simulation = DottedDict()
    self.simulation.waveform = waveform
    self.simulation.hrss = hrss
    self.simulation.right_ascension = right_ascension
    self.simulation.declination = declination
    self.simulation.polarization = polarization
    self.simulation.delay = delay
    self.simulation.strain = 0
    self.simulation.SNR = DottedDict()

    set_logging_message(f"Generated waveform {waveform}")
    # Read sampling rate from first IFO ; assume its the same throughout
    Fs = int(self.data[self.ifos[0]].sample_rate.value)

    # ---- Load waveforms
    # Special case of ad-hoc chirp
    # Check if 'chirp' is in the waveform and 'ISCO' is not
    if "chirp" in waveform and "ISCO" not in waveform:
        try:
            # Extract arguments from waveform string
            args = waveform.split("~")
            duration_sec = float(args[1])
            f0 = float(args[2])
            f1 = float(args[3])
            chirp_method = args[4]
            beta = float(args[5])
        except:
            # Fallback to random parameters if parsing fails
            waveform = np.random.choice(['chirp', 'chirp_r'])
            duration_type = np.random.choice(['long', 'short'])
            duration_sec = np.random.randint(50, 501) if duration_type == 'long' else np.random.randint(10, 51)
            f0, f1 = np.random.randint(30, 2001, size=2)
            chirp_method = np.random.choice(['linear', 'quadratic', 'logarithmic', 'hyperbolic'])
            beta = np.random.uniform(1, 4)
    
        # Common chirp signal generation
        time_array = np.arange(0, int(duration_sec * Fs)) / Fs
        chirp_signal = chirp(time_array, f0=f0, f1=f1, t1=duration_sec, method=chirp_method)
        envelope = kaiser(len(time_array) * 2 - 1, beta=beta)
        max_index = np.argmax(envelope)
    
        # Apply envelope, reverse if necessary
        if "_r" in waveform:
            chirp_signal *= envelope[max_index:][::-1]
        else:
            chirp_signal *= envelope[max_index:]
    
        # Store results in a structured array
        Waveform = np.zeros((len(chirp_signal), 3))
        Waveform[:, 0] = time_array
        Waveform[:, 1] = 1e-21 * chirp_signal  # Adjust amplitude
        Waveform[:, 2] = 0  # Linearly polarized
        filename = waveform  # Keep the filename for further use

    # otherwise load from file
    else:
        # ---- Load the pre-tapered version if possible
        if os.path.exists(
            f"{os.path.expanduser(catalog)}{waveform}_tapered_{Fs}Hz.dat"
        ):
            filename = f"{os.path.expanduser(catalog)}{waveform}_tapered_{Fs}Hz.dat"
        else:
            filename = f"{os.path.expanduser(catalog)}{waveform}_{Fs}Hz.dat"

        # ---- Silently deal with cases where delimiters are space, coma or tab
        try:
            Waveform = iter_loadtxt(filename, delimiter=",")
        except:
            try:
                Waveform = iter_loadtxt(filename, delimiter=" ")
            except:
                Waveform = iter_loadtxt(filename, delimiter="\t")

    if hrss is None and SNR is None:
        try:
            config = ConfigObj(f"{PATH_TO_PYXEL}/efficiency/{self.data.run}")
            self.simulation.hrss = float(config.get(f"{waveform}"))
        except:
            self.simulation.hrss = 1e-21

    for ifo in self.ifos:
        detector = Detector(ifo, reference_time=self.data[ifo].t0.value + delay)

        # ---- Silently deal with cases where column 0 is time
        try:
            h_cross = TimeSeries(
                Waveform[:, 1],
                sample_rate=Fs,
                unit=self.data[ifo].unit,
                t0=self.data[ifo].t0.value + delay,
            )
            h_plus = TimeSeries(
                Waveform[:, 2],
                sample_rate=Fs,
                unit=self.data[ifo].unit,
                t0=self.data[ifo].t0.value + delay,
            )
        except:
            h_cross = TimeSeries(
                Waveform[:, 0],
                sample_rate=Fs,
                unit=self.data[ifo].unit,
                t0=self.data[ifo].t0.value + delay,
            )
            h_plus = TimeSeries(
                Waveform[:, 1],
                sample_rate=Fs,
                unit=self.data[ifo].unit,
                t0=self.data[ifo].t0.value + delay,
            )

        self.simulation.duration = len(h_cross) / Fs

        # ---- Make sure the waveform is tapered
        # KLUDGE: ISCOChirp waveform should be redone; issue with downsampling ?
        if "tapered" not in filename:
            h_cross = h_cross.taper(side="leftright", duration=1)
            h_plus = h_plus.taper(side="leftright", duration=1)
        if "ISCOchirp" in filename:
            h_cross = h_cross.taper(side="left", duration=105)
            h_plus = h_plus.taper(side="left", duration=105)

        # ---- Rescale to requested HRSS
        # .     HRSS is computed before projecting onto detectors with sky pos.
        if SNR is None:
            hrss_default = MeasureHrss(h_plus.to_lal(), h_cross.to_lal())
            h_cross = h_cross * self.simulation.hrss / hrss_default
            h_plus = h_plus * self.simulation.hrss / hrss_default

        # ---- Project onto detector frame with sky position
        simulation = detector.project_wave(
            h_plus.to_pycbc(),
            h_cross.to_pycbc(),
            right_ascension,
            declination,
            polarization,
        )
        simulation = TimeSeries.from_pycbc(simulation)
        simulation.t0 = self.data[ifo].t0.value + delay
        simulation.override_unit(self.data[ifo].unit)

        self.simulation.SNR[ifo] = MeasureSNR(
            simulation.to_lal(),
            self.data[ifo][
                int(delay * self.data[ifo].sample_rate.value) : int(
                    delay * self.data[ifo].sample_rate.value
                )
                + len(h_plus)
            ]
            .psd(0.2,0.1)
            .to_lal(),
            f_min=0,
            f_max=self.data[ifo].sample_rate.value / 2,
        )

        self.simulation[ifo] = DottedDict()
        self.simulation[ifo].h_plus = h_plus
        self.simulation[ifo].h_cross = h_cross
        self.simulation[ifo].ht = simulation

    # ---- compute network SNR
    self.simulation.SNR["network"] = np.sqrt(
        np.array([self.simulation.SNR[ifo] ** 2 for ifo in self.ifos]).sum()
    )

    # ---- rescale to requested netowrk SNR
    if SNR is not None:
        factor = SNR / self.simulation.SNR["network"]
        for ifo in self.ifos:
            self.simulation[ifo].h_plus = factor * self.simulation[ifo].h_plus
            self.simulation[ifo].h_cross = factor * self.simulation[ifo].h_cross
            self.simulation[ifo].ht = factor * self.simulation[ifo].ht

            # ---- recompute single ifo SNR
            self.simulation.SNR[ifo] = MeasureSNR(
                self.simulation[ifo].ht.to_lal(),
                self.data[ifo][
                    int(delay * self.data[ifo].sample_rate.value) : int(
                        delay * self.data[ifo].sample_rate.value
                    )
                    + len(h_plus)
                ]
                .psd(0.2,0.1)
                .to_lal(),
                f_min=0,
                f_max=self.data[ifo].sample_rate.value / 2,
            )

        # ---- recompute the HRSS, taking any ifo (H_plus. and H_cross are the same)
        self.simulation.hrss = MeasureHrss(
            self.simulation[ifo].h_plus.to_lal(),
            self.simulation[ifo].h_cross.to_lal(),
        )

        # ---- recompute network SNR
        self.simulation.SNR["network"] = np.sqrt(
            np.array([self.simulation.SNR[ifo] ** 2 for ifo in self.ifos]).sum()
        )

    # ---- inject waveform
    for ifo in self.ifos:
        if(overwrite=='False'):
            self.data[ifo].override_unit(self.data[ifo].unit)
            self.data[ifo] = self.data[ifo].inject(self.simulation[ifo].ht)
        else:
            self.data_untouched[ifo].override_unit(self.data[ifo].unit)
            self.data[ifo] = self.data_untouched[ifo].inject(self.simulation[ifo].ht)


    self.name = "_".join(
        map(
            "{0}-{1}".format,
            self.ifos,
            [int(self.data[ifo].t0.value) for ifo in self.ifos],
        )
    )
    # ---- add simulation information to the name string. Useful for saving later.
    self.name = f"simulation_{waveform}_" + ("{:0.2e}".format(self.simulation.hrss))+  "_" + self.name 
    self.name += f"_{self.uuid}"


    # ---- compute injection mask
    mask_inj = TimeSeries(
        np.zeros(len(self.data[ifo])),
        sample_rate=Fs,
        unit=self.data[ifo].unit,
        t0=self.data[ifo].t0,
    )
    mask_inj.update(mask_inj.inject(self.simulation[ifo].ht).value)
    self.simulation.mask_ht = mask_inj



    mask,f = self.compute_psd(self.simulation.mask_ht.value)
    mask = mask>1e-48
    self.simulation.mask = np.swapaxes(mask,0,1)
    self.find_injection_mask()

    
