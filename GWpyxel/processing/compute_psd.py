import numpy as np
import scipy.signal
import matplotlib.pyplot as plt
from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message

import logging
logger = logging.getLogger(__name__)

_USE_INI_DEFAULT = object()

@time_logger
def compute_psd(self,signal, fs=_USE_INI_DEFAULT, segment_duration=_USE_INI_DEFAULT, freq_res=_USE_INI_DEFAULT):

    if fs is _USE_INI_DEFAULT: fs = self.config.data.Fs
    if segment_duration is _USE_INI_DEFAULT: segment_duration = self.config.tfmap.tres
    if freq_res is _USE_INI_DEFAULT: freq_res = self.config.tfmap.fres
        
    segment_samples = int(fs * segment_duration)
    nfft = int(fs / freq_res)
    psd_values = []

    for start in range(0, len(signal), segment_samples):
        end = start + segment_samples
        if end > len(signal):
            break

        segment = signal[start:end]
        f, Pxx = scipy.signal.welch(segment, fs, window='hann', nperseg=nfft, detrend='constant')
        psd_values.append(Pxx)

    return np.array(psd_values).T, f