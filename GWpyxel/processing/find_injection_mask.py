import logging
logger = logging.getLogger(__name__)


_USE_INI_DEFAULT = object()

from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message
@time_logger
def find_injection_mask(self):

    import numpy as np
    from skimage.filters import sobel
    from scipy.ndimage import binary_dilation
    
    self.compute_coherence(self.data.H1.value,self.data.L1.value)
    coherence_map = np.swapaxes(self.spectrogram.value,0,1)
    
    psd_map,f = self.compute_psd(self.simulation.mask_ht.value)
    
    # Compute the gradient magnitude of the image
    gradient_magnitude = sobel(coherence_map)
    mask = psd_map>1e-48
    
    # Find the boundary pixels of the mask
    boundary_mask = binary_dilation(mask) ^ mask
    
    # Initialize an array for edge strengths
    edge_strengths = np.zeros_like(coherence_map)
    
    # Calculate edge strength for each pixel inside the mask
    for (y, x), is_masked in np.ndenumerate(mask):
        if is_masked:
            max_strength = 0
            # Check the adjacent pixels
            for dy in range(-1, 2):
                for dx in range(-1, 2):
                    ny, nx = y + dy, x + dx
                    # Ensure we're not out of bounds and the neighbor is outside the mask
                    if (0 <= ny < mask.shape[0] and 0 <= nx < mask.shape[1] and not mask[ny, nx]):
                        edge_strength = abs(gradient_magnitude[y, x] - gradient_magnitude[ny, nx])
                        max_strength = max(max_strength, edge_strength)
            edge_strengths[y, x] = max_strength
    
    binary = edge_strengths > 0.035
    
    import hdbscan
    
    # Convert the mask into a set of points
    points = np.argwhere(binary)
    
    # Run HDBSCAN
    try:
        clusterer = hdbscan.HDBSCAN(min_cluster_size=6, gen_min_span_tree=True)
        cluster_labels = clusterer.fit_predict(points)
        
        # You can use clusterer.minimum_spanning_tree_ to visualize the clustering
        # clusterer.single_linkage_tree_.plot(cmap='viridis', colorbar=True)
        
        # Now, filter out small clusters/noise and create a new mask
        filtered_mask = np.zeros_like(binary)
        for label in set(cluster_labels):
            if label != -1:  # -1 is the label for 'noise' in HDBSCAN
                # Set all points in this cluster to 1
                cluster_points = points[cluster_labels == label]
                filtered_mask[cluster_points[:, 0], cluster_points[:, 1]] = 1
        
        from skimage import morphology
        filtered_mask = morphology.remove_small_objects(filtered_mask, 3)
        final_mask = filtered_mask
        
        # Find the coordinates of the non-zero values of the mask
        coords = np.argwhere(final_mask)
        
        # Determine the bounding box of the non-zero elements
        x_min, y_min = coords.min(axis=0)
        x_max, y_max = coords.max(axis=0)
        
        # Create an array filled with zeros, with the same shape as the mask
        rectangle_mask = np.zeros_like(final_mask)
        
        # Fill in the rectangle within the bounding box with ones
        rectangle_mask[x_min:x_max+1, y_min:y_max+1] = 1
    
        
        self.simulation.mask_spectrogram = np.swapaxes(rectangle_mask*mask,0,1)
    except:
        self.simulation.mask_spectrogram = np.swapaxes(np.zeros_like(mask),0,1)
