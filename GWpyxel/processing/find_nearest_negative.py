
def find_nearest_negative(self, list_array, value):
    value_rounded = round(value, 3)
    list_array_rounded = [round(x, 3) for x in list_array]
    list_reduced = [x - value_rounded for x in list_array_rounded]
    list_negative = [x for x in list_reduced if x<0]
    if(list_negative==[]):
        return 0, 0
    nearest_negative = max(list_negative)+value_rounded
    idx_nearest = list_array_rounded.index(round(nearest_negative,3))
    return nearest_negative, idx_nearest